"""src URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
import src.adm.views
import src.login.views

urlpatterns = [
    # url(r'^.*', src.administration.views.catchUrl, name='catchUrl'),
    url(r'^$', src.adm.views.index_page, name='index_page'),
    url(r'^adm/$', src.adm.views.index_page,
        name='index_page'),
    url(r'^form/survey$', src.adm.views.form_page,
        name='form_page'),
    url(r'^form/herbal$', src.adm.views.herbal_form_page,
        name='herbal_form_page'),
    url(r'^form/search$', src.adm.views.search_form_page,
        name='search_form_page'),
    url(r'^form/users$', src.adm.views.users_form_page,
        name='users_form_page'),
    url(r'^form/herbal_management$', src.adm.views.herbal_management_page,
        name='herbal_management_page'),
    url(r'^form/deseases_management$', src.adm.views.deseases_management_page,
        name='deseases_management_page'),
    url(r'^form/form_management$', src.adm.views.form_management_page,
        name='form_management_page'),
        
    
    # data fetcher
    url(r'fetchdataquery/(?P<table>.*)/(?P<query>.*)$', src.adm.views.fetchDataQuery,
        name='fetchDataQuery'),
    url(r'fetchdataquery/(?P<table>.*)/$', src.adm.views.fetchDataQuery,
         name='fetchDataQuery'),
    url(r'fetchalldata/(?P<table>.*)/$', src.adm.views.fetchAllData,
        name='fetchAllData'),

    url(r'saveform/$', src.adm.views.saveform,
        name='saveform'),
    url(r'getforms/$', src.adm.views.getforms,
        name='getforms'),
    url(r'getform', src.adm.views.getform,
        name='getform'),
]
