from django.shortcuts import render, render_to_response
from django.http import HttpResponse
from src.login.models import *
from src.login.models import *
from django.core import serializers
import json
from src.adm.models import *


def index_page(request):
    return render(request, 'main.html', {})


def form_page(request):
    if request.method == "GET" and "id" in request.GET:
        id = request.GET["id"] if request.GET["id"].isnumeric() else None
        return render(request, 'components/form.html', {"formId":id})
        
    return render(request, 'components/form.html', {})


def herbal_form_page(request):
    return render(request, 'components/herbal_form.html', {})


def search_form_page(request):
    return render(request, 'components/search_form.html', {})


def users_form_page(request):
    return render(request, 'components/users_form.html', {})


def herbal_management_page(request):
    return render(request, 'components/herbal_management.html', {})


def deseases_management_page(request):
    return render(request, 'components/deseases_management.html', {})


def form_management_page(request):
    return render(request, 'components/form_management.html', {})


# logical request functions 

def fetchDataQuery(request, table, query="default"):
    # fetch data from table with condition
    if request.method == "GET":

        data = None
        success = True
        results = [{
                    "name"  : "------------",
                    "value" : "(@!?@)|",
                    "text"  : "------------"
                }]

        if query != "default":
            if table == "interviewer":
                data = Interviewer.objects.filter(
                    description__icontains=query
                    ).order_by("description")
            elif table == "province":
                data = Province.objects.filter(
                    description__icontains=query
                    ).order_by("description")
            elif table == "village":
                data = Village.objects.filter(
                    description__icontains=query
                    ).order_by("description")
            elif table == "diseases":
                data = Diseases.objects.filter(
                    description__icontains=query
                    ).order_by("description")
            elif table == "plants-commonname":
                data = Plants.objects.filter(
                    plant__icontains=query
                    ).order_by("plant")
        else:
            if table == "interviewer":
                data = Interviewer.objects.all().order_by("description")
            elif table == "province":
                data = Province.objects.all().order_by("description")
            elif table == "village":
                data = Village.objects.all().order_by("description")
            elif table == "diseases":
                data = Diseases.objects.all().order_by("description")
            elif table == "plants-commonname":
                data = Plants.objects.all().order_by("plant")

        if data:  
            for i in range(len(data)):
                if table != "plants-commonname":
                    if data[i].description == "" or len(data[i].description) == 0:
                        continue 
                    name = data[i].description
                    value = data[i].id
                    text = data[i].description
                elif table == "plants-commonname":
                    if data[i].plant == "" or len(data[i].plant) == 0:
                        continue
                    name = data[i].plant
                    value = data[i].id
                    text = data[i].plant

                results.append({
                    "name"  : name,
                    "value" : value,
                    "text"  : text
                })
        else:
            success = False
        
        if len(query) > 0:
            results.append({
                "name"  : query + " (Nuevo)",
                "value" : "(@!?@)|"+query,
                "text"  : query + " (Nuevo)"
            })

    return HttpResponse(json.dumps({
        "success": success,
        "results": results
    }))


def fetchAllData(request, table):
    # fetch all data of a table
    if request.method == "GET":

        data = None
        success = True
        results = [{
                    "name"  : "------------",
                    "value" : "new",
                    "text"  : "------------"
                }]

        if table == "interviewer":
            data = Interviewer.objects.all().order_by("description")
        elif table == "province":
            data = Province.objects.all().order_by("description")
        elif table == "village":
            data = Village.objects.all().order_by("description")
        elif table == "diseases":
            data = Diseases.objects.all().order_by("description")
        elif table == "plants-commonname":
            data = Plants.objects.all().order_by("plant")

        if data:
            for i in range(len(data)):
                if table != "plants-commonname":
                    if data[i].description == "" or len(data[i].description) == 0:
                        continue
                    name = data[i].description
                    value = data[i].id
                    text = data[i].description
                elif table == "plants-commonname":
                    if data[i].plant == "" or len(data[i].plant) == 0:
                        continue
                    name = data[i].plant
                    value = data[i].id
                    text = data[i].plant

                results.append({
                    "name"  : name,
                    "value" : value,
                    "text"  : text
                })
        else:
            success = False

    return HttpResponse(json.dumps({
        "success": success,
        "results": results
    }))



# Save Quiz Form Data
def saveform(request):
    if request.method == "POST":
        success = True
        msg = None
        try:
            request.POST = { k:v.strip() for k, v in request.POST.iteritems()}

            # Nuevos valores de las tablas de control se envian con el prefix (@!?@)| + valor para su distincion
            main_form = request.POST["main_form"]
            main_form = 1 if main_form == "true" else 0

            form_date = request.POST["form_date"]
            form_date = None if form_date == "None" else form_date

            interviewer = request.POST["interviewer"].capitalize()
            interviewed_name = request.POST["interviewed_name"].capitalize()
            interviewed_lastname = request.POST["interviewed_lastname"].capitalize()
            interviewed_occupation = request.POST["interviewed_occupation"].capitalize()
            interviewed_gender = request.POST["interviewed_gender"]
            interviewed_province = request.POST["interviewed_province"]
            interviewed_village = request.POST["interviewed_village"]
            interviewed_education = request.POST["interviewed_education"]
            sick_person = request.POST["sick_person"].capitalize()
            person_disease = request.POST["person_disease"]
            person_symptom = request.POST["person_symptom"].split(",")
            person_mt = request.POST["person_mt"]
            knowabaout_other_plant = request.POST["knowabaout_other_plant"]
            treatments_obj = json.loads(request.POST["treatments"])

            # saving main form data control
            interviewer = saveControlData("Interviewer",interviewer)
            interviewed_province = saveControlData("Province",interviewed_province)
            interviewed_village = saveControlData("Village",interviewed_village)
            person_disease = saveControlData("Diseases",person_disease) # disease
            knowabaout_other_plant = saveControlData("Diseases",knowabaout_other_plant)
            
            # interviewed - not required fields
            interviewed = Interviewed.objects.create(
                name = interviewed_name,
                last_name = interviewed_lastname,
                gender = interviewed_gender,
                occupation = interviewed_occupation,
                education = interviewed_education,
                province_id = interviewed_province,
                village_id = interviewed_village
            )

            # form
            form = Form.objects.create(
                interviewer_id = interviewer,
                interviewed_id = interviewed.id,
                disease_id = person_disease,
                sick_person = sick_person,
                measures_taken = person_mt,
                other_diseases = knowabaout_other_plant,
                date = form_date,
                main_form = main_form
            )

            # symptons
            for i in range(len(person_symptom)):
                if len(person_symptom[i]) > 0:
                    symptoms = Symptoms.objects.create(
                        description = person_symptom[i],
                        disease_id = person_disease,
                        form = form
                    )

            # treatments
            for i in range(len(treatments_obj)):
                # data control
                disease = saveControlData("Diseases", treatments_obj[i]["disease"]) # disease
                plant_uses_plant = saveControlData("Plants", treatments_obj[i]["plant_uses_plant"])

                treatments = Treatments.objects.create(
                    disease_id = disease,
                    plant_id = plant_uses_plant,
                    plant_other_name = treatments_obj[i]["plant_uses_othername"],
                    plant_use = treatments_obj[i]["history_use_results"],
                    plant_origin = treatments_obj[i]["ethnobotany_plant_origin"],
                    plant_existence = treatments_obj[i]["ethnobotany_plant_existence"],
                    plant_picking = treatments_obj[i]["ethnobotany_plant_picking"],
                    plant_used_parts = treatments_obj[i]["plant_uses_used_parts"],
                    treatment = treatments_obj[i]["plant_uses_treatment"],
                    treatment_origin = treatments_obj[i]["history_treatment_origin"],
                    dose = treatments_obj[i]["plant_uses_dose"],
                    dose_children = treatments_obj[i]["plant_uses_dose_children"],
                    dose_pregnant = treatments_obj[i]["plant_uses_dose_pregnant"],
                    form_id = form.id
                )

                # saving treatment applications
                applications = treatments_obj[i]["plant_uses_application"].split(",")
                for ix in range(len(applications)):
                    if len(applications[ix]) > 0:
                        application = saveControlData("Applications", applications[ix])
                        TreatmentApplications.objects.create(
                            application_id = application,
                            treatment_id = treatments.id
                        ) 

            success = True
        except Exception as ex:
            success = False
            msg = str(ex)
            print "Error: "+msg


    return HttpResponse(json.dumps({
        "success": success,
        "msg": msg
    }))


def saveControlData(table, value):
    
    if value.isnumeric(): # is passing object id
        return int(value)
    else:
        try:
            value = value.replace("(@!?@)|","").capitalize() # removing prefix
            if table == "Interviewer":
                kwargs = {"description":value}
                obj, created = filter_or_create(Interviewer,kwargs)

            elif table == "Diseases":
                kwargs = {"description":value}
                obj, created = filter_or_create(Diseases,kwargs)

            elif table == "Plants":
                kwargs = {"plant":value}
                obj, created = filter_or_create(Plants,kwargs)

            elif table == "Species":
                kwargs = {"description":value}
                obj, created = filter_or_create(Species,kwargs)

            elif table == "Families":
                kwargs = {"description":value}
                obj, created = filter_or_create(Families,kwargs)

            elif table == "Genders":
                kwargs = {"description":value}
                obj, created = filter_or_create(Genders,kwargs)

            elif table == "Authors":
                kwargs = {"description":value}
                obj, created = filter_or_create(Authors,kwargs)

            elif table == "Flowers":
                kwargs = {"description":value}
                obj, created = filter_or_create(Flowers,kwargs)

            elif table == "Province":
                kwargs = {"description":value}
                obj, created = filter_or_create(Province,kwargs)
    
            elif table == "Village":
                kwargs = {"description":value}
                obj, created = filter_or_create(Village,kwargs)
   
            elif table == "Applications":
                kwargs = {"description":value}
                obj, created = filter_or_create(Applications,kwargs)

            elif table == "Collectors":
                kwargs = {"description":value}
                obj, created = filter_or_create(Collectors,kwargs)

            elif table == "Determiners":
                kwargs = {"description":value}
                obj, created = filter_or_create(Determiners,kwargs)

            else:
                print "No se encontro la tabla: "+table
            
            if not created:
                return obj.first().id
            else:
                return obj.id     

        except Exception as ex:
            print "Error: No se pudo guardar valor en "+table+". "+str(ex)
            return False


def filter_or_create(Obj_model, kwargs):
    created = False
    obj = None
    obj = Obj_model.objects.filter(**kwargs)
    if not obj:
        obj = Obj_model.objects.create(**kwargs)
        created = True
    return obj, created


# Load Quiz Form Data (Individual)
def getform(request):
    if request.method == "POST":
        success = False
        msg = None
        try:
            formId = request.POST["formId"]
            result = []
            data_form = list(Form.objects.prefetch_related("interviewer").
                                prefetch_related("interviewed").
                                prefetch_related("disease").filter(id=formId))

            for i in range(len(data_form)):
                province = list(Province.objects.filter(id=data_form[i].interviewed.province_id))
                village = list(Village.objects.filter(id=data_form[i].interviewed.province_id))
                treatment = list(Treatments.objects.prefetch_related("plant").
                                prefetch_related("disease").filter(form_id=data_form[i].id))
                other_disease = list(Diseases.objects.filter(id=data_form[i].other_diseases))

                # INCOMPLETE
                symptoms = list(Symptoms.objects.filter(
                        disease_id=data_form[i].disease.id,
                        form_id=data_form[i].id
                    ))

                # INCOMPLETE
                symptoms_arr = []
                for sym in range(len(symptoms)):
                    symptoms_arr.append({
                        "id":symptoms[sym].id,
                        "desc":symptoms[sym].description,
                        "duration":symptoms[sym].symptom_duration
                    })

                treatments = []
                for j in range(len(treatment)):
                    treat_applic = list(TreatmentApplications.objects.prefetch_related("application").filter(treatment_id=treatment[j].id))
                    applications_arr = []
                    for index in range(len(treat_applic)):
                        applications_arr.append(treat_applic[index].application.description)

                    treatments.append({
                        "id": treatment[j].id,
                        "disease_id": treatment[j].disease.id,
                        "disease_desc": treatment[j].disease.description,
                        "plant_id": treatment[j].plant.id,
                        "plant_name": treatment[j].plant.plant,
                        "plant_other_name": treatment[j].plant_other_name,
                        "plant_use": treatment[j].plant_use,
                        "plant_origin": treatment[j].plant_origin,
                        "plant_existence": treatment[j].plant_existence,
                        "plant_picking": treatment[j].plant_picking,
                        "plant_used_parts": treatment[j].plant_used_parts,
                        "treatment": treatment[j].treatment,
                        "treatment_origin": treatment[j].treatment_origin,
                        "dose": treatment[j].dose,
                        "dose_children": treatment[j].dose_children,
                        "dose_pregnant": treatment[j].dose_pregnant,
                        "applications": applications_arr,
                        "form_id": treatment[j].form_id
                    })

                result.append({
                    "id": data_form[i].id,
                    "interviewer_id": data_form[i].interviewer.id,
                    "interviewer_name": data_form[i].interviewer.description,
                    "interviewed_id": data_form[i].interviewed.id,
                    "interviewed_name": data_form[i].interviewed.name,
                    "interviewed_lastname": data_form[i].interviewed.last_name,
                    "interviewed_gender": data_form[i].interviewed.gender,
                    "interviewed_occupation": data_form[i].interviewed.occupation,
                    "interviewed_education": data_form[i].interviewed.education,
                    "interviewed_province_id": data_form[i].interviewed.province_id,
                    "interviewed_village_id": data_form[i].interviewed.village_id,
                    "interviewed_province_desc": province[0].description,
                    "interviewed_village_desc": village[0].description,
                    "disease_id": data_form[i].disease.id,
                    "disease_desc": data_form[i].disease.description,
                    "sick_person": data_form[i].sick_person,
                    "symptoms": symptoms_arr,
                    "measures_taken": data_form[i].measures_taken,
                    "knowabaout_other_plant_id": data_form[i].other_diseases,
                    "knowabaout_other_plant_desc": other_disease[0].description,
                    "treatments": treatments,
                    "date": str(data_form[i].date),
                    "mainForm": data_form[i].main_form
                })
            
            success = True if len(result) > 0 else False

        except Exception as ex:
            success = False
            msg = str(ex)
            print msg
        
    return HttpResponse(json.dumps({
        "success": success,
        "msg": msg,
        "data" : result
    }))


# Load All Quiz Form
def getforms(request):
    if request.method == "POST":
        success = True
        msg = None
        rows_count = 0
        try:
            formId = request.POST["formId"]
            init = int(request.POST["init"])
            end = int(request.POST["end"])
            result = []

            if not formId == "all":
                data = Form.objects.filter(id=formId)
                rows_count = data.count()
            else:
                data = list(Form.objects.all())
                rows_count = len(data)
                data = data[init:end]

            

            for i in range(len(data)):
                result.append({
                    "id": data[i].id,
                    "interviewerId": data[i].interviewer_id,
                    "interviewedId": data[i].interviewed_id,
                    "diseaseId": data[i].disease_id,
                    "sickPerson": data[i].sick_person,
                    "date": str(data[i].date),
                    "mainForm": data[i].main_form
                })
            success = True
        except Exception as ex:
            success = False
            msg = str(ex)
            print msg 

    return HttpResponse(json.dumps({
        "success": success,
        "msg": msg,
        "data" : result,
        "rows_count": rows_count
    }))