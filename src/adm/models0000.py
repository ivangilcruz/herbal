# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Applications(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'applications'


class Authors(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'authors'


class ClasificacionEnLatin(models.Model):
    id = models.AutoField(primary_key=True)
    latin_id = models.IntegerField(db_column='Latin ID', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    familia = models.CharField(db_column='Familia', max_length=10, blank=True, null=True)  # Field name made lowercase.
    genero = models.CharField(db_column='Genero', max_length=14, blank=True, null=True)  # Field name made lowercase.
    especie = models.CharField(db_column='Especie', max_length=14, blank=True, null=True)  # Field name made lowercase.
    autor = models.CharField(db_column='Autor', max_length=17, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'clasificacion_en_latin'


class Collectors(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'collectors'


class Determiners(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'determiners'


class DiseaseCategories(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'disease_categories'


class Diseases(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField(blank=True, null=True)
    category = models.IntegerField(blank=True, null=True)
    symtoms = models.TextField(blank=True, null=True)
    symptom_duration = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'diseases'


class Families(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'families'


class Flowers(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'flowers'


class Form(models.Model):
    id = models.AutoField(primary_key=True)
    interviewer_id = models.IntegerField(blank=True, null=True)
    interviewed_id = models.IntegerField(blank=True, null=True)
    disease_id = models.IntegerField(blank=True, null=True)
    sick_person = models.TextField(blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    main_form = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'form'


class Genders(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'genders'


class Herbal(models.Model):
    id = models.AutoField(primary_key=True)
    family_id = models.IntegerField(blank=True, null=True)
    gender_id = models.IntegerField(blank=True, null=True)
    specie_id = models.IntegerField(blank=True, null=True)
    author_id = models.IntegerField(blank=True, null=True)
    plant_id = models.IntegerField(blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    number = models.IntegerField(blank=True, null=True)
    plant_height = models.TextField(blank=True, null=True)
    village_id = models.IntegerField(blank=True, null=True)
    province_id = models.IntegerField(blank=True, null=True)
    flower_description_id = models.IntegerField(blank=True, null=True)
    observations = models.TextField(blank=True, null=True)
    collector_id = models.IntegerField(blank=True, null=True)
    determiner_id = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'herbal'


class Interviewed(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField(blank=True, null=True)
    last_name = models.TextField(blank=True, null=True)
    gender = models.TextField(blank=True, null=True)
    occupation = models.TextField(blank=True, null=True)
    place = models.TextField(blank=True, null=True)
    education = models.TextField(blank=True, null=True)
    province_id = models.IntegerField(blank=True, null=True)
    village_id = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'interviewed'


class Interviewer(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'interviewer'


class Plants(models.Model):
    id = models.AutoField(primary_key=True)
    plant = models.TextField(blank=True, null=True)
    common_name = models.TextField(blank=True, null=True)
    latin_name = models.TextField(blank=True, null=True)
    scientific_name = models.TextField(blank=True, null=True)
    other_name = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'plants'


class Province(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'province'


class Species(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'species'


class Symptoms(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField(blank=True, null=True)
    disease_id = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'symptoms'


class Treatments(models.Model):
    id = models.AutoField(primary_key=True)
    disease_id = models.IntegerField(blank=True, null=True)
    plant_id = models.IntegerField(blank=True, null=True)
    plant_use = models.TextField(blank=True, null=True)
    plant_origin = models.TextField(blank=True, null=True)
    plant_existence = models.TextField(blank=True, null=True)
    plant_picking = models.TextField(blank=True, null=True)
    plant_used_parts = models.TextField(blank=True, null=True)
    treatment = models.TextField(blank=True, null=True)
    treatment_origin = models.TextField(blank=True, null=True)
    application_id = models.IntegerField(blank=True, null=True)
    another_application_id = models.IntegerField(blank=True, null=True)
    dose = models.TextField(blank=True, null=True)
    dose_children = models.TextField(blank=True, null=True)
    dose_pregnant = models.TextField(blank=True, null=True)
    form_id = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'treatments'


class Village(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'village'
