FROM python:2

RUN mkdir /herbario
WORKDIR /herbario

ADD requirements.txt /herbario/
RUN pip install --no-cache-dir -r requirements.txt

ADD . /herbario
